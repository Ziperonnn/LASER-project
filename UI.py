import tkinter as tk
from fonctions_ui import *
from tkinter import ttk


def initialisation_UI():
    # création d'une UI
    root = tk.Tk()
    # nom de l'UI
    root.title('Projet Optique : LASER ')
    # affectation d’une image sur l'UI
    root.iconphoto(True, tk.PhotoImage(file='Ressources/logo/laser_pointer.png'))
    # définition de la taille de l'UI
    root.geometry('1520x640')
    # changement de couleur de l'arrière-plan
    root['background'] = '#1D2D50'
    # bloquage de l'agrandissement de l'UI
    root.resizable(width=False, height=False)
    # centrage de l'UI au milieu de notre écran
    center(root)
    bool_ct = IntVar()

    return [root, bool_ct]


def erreur_UI(root):
    # erreur sur le type de variable qu’est lambda
    text_error = Label(root, text='Error : \n Lambda must be\n a float', bg='#D12323')
    text_error.grid(row=2, column=3, padx=5, pady=5)
    text_error.grid_forget()

    # erreur sur la valeur de lambda
    text_error_l = Label(root, text='Error : \n Lambda must be between\n 150nm and 1,45µm', bg='#D12323')
    text_error_l.grid(row=2, column=3, padx=5, pady=5)
    text_error_l.grid_forget()

    # erreur sur le paramètre de profondeur ou de temps
    text_error_pt = Label(root, text='Error : \n Depth or Time must\n be a number', bg='#D12323')
    text_error_pt.grid(row=2, column=3, padx=5, pady=5)
    text_error_pt.grid_forget()

    # erreur sur le paramètre d'intensité initial
    text_error_II = Label(root, text='Error : \n Initial Intensity must\n be a number', bg='#D12323')
    text_error_II.grid(row=2, column=3, padx=5, pady=5)
    text_error_II.grid_forget()

    return [text_error, text_error_l, text_error_pt, text_error_II]


def affichage_entrée(root, bool_ct):
    # labels longueur d'onde de travail
    label_wl = Label(root, text="Longueur d'onde (m) \n [entre 150nm et 1450nm]", bg='#1E5F74', fg='#FCDAB7')
    label_wl.grid(row=0, column=0, padx=20, pady=5)
    # entrée longueur d'onde de travail
    wl_entry = Entry(root, width=20, bg='#E6BA95')
    wl_entry.grid(row=1, column=0, padx=20, pady=5)

    # labels profondeur d’étude sur le matériau
    label_pf = Label(root, text="Profondeur (µm) \n [option]", bg='#1E5F74', fg='#FCDAB7')
    label_pf.grid(row=0, column=1, padx=20, pady=5)
    # entrée profondeur d’étude sur le matériau
    profondeur = Entry(root, width=20, bg='#E6BA95')
    profondeur.grid(row=1, column=1, padx=20, pady=5)

    # labels temps d’étude sur le matériau
    label_tps = Label(root, text="temps (ms) \n [option]", bg='#1E5F74', fg='#FCDAB7')
    label_tps.grid(row=0, column=2, padx=20, pady=5)
    # entrée temps d’étude sur le matériau
    temps = Entry(root, width=20, bg='#E6BA95')
    temps.grid(row=1, column=2, padx=20, pady=5)

    # labels nom du graphe sur le matériau
    label_nom_graph = Label(root, text="Nom de la carte de température", bg='#1E5F74', fg='#FCDAB7')
    label_nom_graph.grid(row=0, column=3, padx=20, pady=5)
    # entrée nom graphique sur le matériau
    nom_graph = Entry(root, width=20, bg='#E6BA95')
    nom_graph.grid(row=1, column=3, padx=20, pady=5)

    # labels temps d’étude sur le matériau
    label_intensity = Label(root, text="Intensité Initiale (en W/m2)", bg='#1E5F74', fg='#FCDAB7')
    label_intensity.grid(row=2, column=0, padx=20, pady=5)
    # entrée temps d’étude sur le matériau
    Intensity = Entry(root, width=20, bg='#E6BA95')
    Intensity.grid(row=3, column=0, padx=20, pady=5)

    # création d’une liste déroulante pour le choix des caractéristiques d’un matériau
    labelChoix_mat = tk.Label(root, text="Veuillez faire un choix de matériau : ", bg='#1E5F74', fg='#FCDAB7')
    labelChoix_mat.grid(row=0, column=4, padx=20, pady=5)
    # 1) - Création la liste Python contenant les éléments de la liste Combobox
    box_mat = ["Aluminium", "Graphite", "Silicium"]
    # 2) - Création de la Combobox via la méthode ttk.Combobox()
    listeCombo_mat = ttk.Combobox(root, values=box_mat)
    # On évite la modification des boxs
    listeCombo_mat['state'] = 'readonly'
    # 3) - Choisir l'élément qui s'affiche par défaut
    listeCombo_mat.set('Sélectionner')
    listeCombo_mat.grid(row=1, column=4, padx=20, pady=5)

    # création d’une liste déroulante pour le choix des caractéristiques d’un matériau
    labelChoix_fct = tk.Label(root, text="Veuillez faire un choix\nde la forme temporelle pour l'impulsion laser : ",
                              bg='#1E5F74', fg='#FCDAB7')
    labelChoix_fct.grid(row=0, column=5, padx=20, pady=5)
    # 1) - Création la liste Python contenant les éléments de la liste Combobox
    box_fct_temps = ["Fonction Porte", "Fonction Gaussienne"]
    # 2) - Création de la Combobox via la méthode ttk.Combobox()
    listeCombo_fct = ttk.Combobox(root, values=box_fct_temps)
    # On évite la modification des boxs
    listeCombo_fct['state'] = 'readonly'
    # 3) - Choisir l'élément qui s'affiche par défaut
    listeCombo_fct.set('Sélectionner')
    listeCombo_fct.grid(row=1, column=5, padx=20, pady=5)

    # Création d'une checkbox pour le choix d’une capacité thermique constante ou dépendante de la température
    Checkbutton(root, text="Capacité thermique constante", variable=bool_ct, bg='#1E5F74', fg='#FCDAB7', onvalue=1,
                offvalue=0).grid(row=0, column=6, padx=20, pady=5)

    # Création du point d'accroche de l'image Carte Température
    label_CT = Label(root)
    label_CT.grid(row=2, column=0, columnspan=4, pady=3)
    label_CT.grid_forget()

    # Création du point d'accroche de l'image courbes dans le temps de l'UI
    label_T = Label(root)
    label_T.grid(row=2, column=4, columnspan=4, pady=3)
    label_T.grid_forget()

    return [label_T, label_CT, temps, profondeur, Intensity, nom_graph, wl_entry, listeCombo_fct, listeCombo_mat]


def affichage_sortie(root):
    # sortie de la température maximale obtenue
    Temp_max = Label(root, text='Température maximale\nobtenue (K)', bg='#1E7464', fg='#FCDAB7')
    Temp_max.grid(row=2, column=1, padx=20, pady=5)
    Tmax = Label(root, width=20, bg='#E6BA95', relief='ridge')
    Tmax.grid(row=3, column=1, padx=20, pady=5)

    # sortie de la température moyenne de la dernière couche
    Temp_mean = Label(root, text='Température moyenne \nà la dernière couche (K)', bg='#1E7464', fg='#FCDAB7')
    Temp_mean.grid(row=2, column=2, padx=20, pady=5)
    Tmean = Label(root, width=20, bg='#E6BA95', relief='ridge')
    Tmean.grid(row=3, column=2, padx=20, pady=5)

    # Fluence obtenue
    Fluence = Label(root, text='Fluence du laser (TJ/m2)', bg='#1E7464', fg='#FCDAB7')
    Fluence.grid(row=2, column=3, padx=20, pady=5)
    Fluence_output = Label(root, width=20, bg='#E6BA95', relief='ridge')
    Fluence_output.grid(row=3, column=3, padx=20, pady=5)

    return Tmax, Tmean, Fluence_output

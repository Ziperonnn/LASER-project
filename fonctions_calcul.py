import numpy as np
import csv
from scipy import interpolate


def initialisation(zmax, tmax):
    # Fonction qui retourne toutes les données principales

    # Définitions du pas de temps et d'espace, en unité SI
    Dt = 0.4e-6
    Dz = 10e-6

    nt = 1000  # nombre de point sauvegardé

    # Définition du vecteur contenant les points d’espace
    z = np.arange(0, zmax, Dz)

    # Définition du vecteur contenant les points de temps
    times = np.linspace(0, tmax, num=nt)

    # Définition de la température initiale du matériau à 300K (soit 26,85 °C)
    T0 = 300
    T = T0 * np.ones([len(z), len(times)])

    return Dt, Dz, z, tmax, times, T, T0


def calcul_alpha(ni, lambda_0):
    # Fonction qui calcule du paramètre lambda pour le vecteur intensité
    return (4 * np.pi * ni) / lambda_0


def vect_intensity(I0, Z, lambda_0, nr, ni):
    # Fonction qui calcule le vecteur intensité pour connaître
    # l'évolution de la température dans le matériau

    # appel au calcul du paramètre alpha
    alpha = calcul_alpha(ni, lambda_0)

    # Calcul du coefficient de réflexion du matériau choisi
    R = ((nr - 1) ** 2 + ni ** 2) / ((nr + 1) ** 2 + ni ** 2)

    Intensity = (1 - R) * I0 * np.exp(-alpha * Z)
    return Intensity


def carree_int(t, t1, t2, I0):
    # fonction permettant de calculer l'intensité envoyé par le laser
    # pour une forme temporelle en porte
    if np.logical_and(t > t1, t < t2):
        return I0
    else:
        return 0


def gaussian_int(t, I0, tmax, sigma):
    # fonction permettant de calculer l'intensité envoyé par le laser
    # pour une forme temporelle gaussienne
    return (I0 * 2.5 * 10e-4 / (sigma * np.sqrt(2 * np.pi))) * \
           np.exp(-(((t - tmax) ** 2) / (2 * (sigma ** 2))))


def carree_int_test(t, t1, t2, I0):
    # fonction utilisée pour comparer les deux formes temporelles (porte & gaussienne)
    vect = []
    for x in t:
        if np.logical_and(x > t1, x < t2):
            vect.append(I0)
        else:
            vect.append(0)
    return vect


def cp_alu(T):
    # fonction permettant de calculer la capacité thermique massique en fonction de la température
    # pour l'aluminium
    return (0.7989 + 3e-4 * T - 9e-8 * T ** 2) * 10 ** 3


def cp_graphite(T):
    # fonction permettant de calculer la capacité thermique massique en fonction de la température
    # pour le graphite
    return (-0.11511 + 2.8168e-3 * T) * 10 ** 3


def cp_silicium(T):
    # fonction permettant de calculer la capacité thermique massique en fonction de la température
    # pour le silicium
    return (5.72 + 0.59e-3 * T - 0.99e-5 * T ** 2) * (4.184 / 28.0855e-3)  # conversion en unité SI


def calcul_indice(x, i):
    # Fonction permettant le calcul de l'indice réel et imaginaire du matériau choisi en fonction
    # de la longueur d'onde choisie

    # initialisation des listes utilisées pour récupérer les valeurs d'un csv afin de réaliser une
    # interpolation. De cette interpolation, on peut récuperer n'importe quel indice
    # entre 150nm and 1,45µm
    wavelength, ni, k = [], [], []

    n = 0
    TargetFile = 'Ressources/index/' + i + '_index.csv'
    # Ouvrir le fichier csv
    with open(TargetFile, 'r') as f:
        # Créer un objet csv à partir du fichier
        obj = csv.reader(f, delimiter=',')

        for ligne in obj:
            if n > 0:
                # on ajoute toutes les valeurs du csv dans les listes initialisées plus tôt
                wavelength.append(float(ligne[0] + '.' + ligne[1]))
                ni.append(float(ligne[2] + '.' + ligne[3]))
                k.append(float(ligne[4] + '.' + ligne[5]))
            n += 1

    tck = interpolate.splrep(wavelength, ni)
    tck2 = interpolate.splrep(wavelength, k)
    return [interpolate.splev(x, tck), interpolate.splev(x, tck2)]

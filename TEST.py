from fonctions_calcul import *
from matplotlib import pyplot
import csv
from scipy import interpolate

'''nr = 3.6941  # Indice réel
ni = 0.0065435
R = ((nr - 1) ** 2 + ni ** 2) / ((nr + 1) ** 2 + ni ** 2)
print(R)
'''

Dt, Dz, z, tmax, times, T, T0 = initialisation(1e-3, 10e-3)

I01 = 10

print('tmax vaut ', tmax)
Y_gaussian = gaussian_int(times, I01/10, 2e-3, 0.0001)  #
Y_porte = carree_int_test(times, 1.875e-3, 2.125e-3, I01)

ga = np.trapz(Y_gaussian, dx=0.1)
da = np.trapz(Y_porte, dx=0.1)

print('g aera  = ', ga)
print('d aera  = ', da)

pyplot.plot(times, Y_gaussian)
pyplot.plot(times, Y_porte)
pyplot.xlabel('Temps (ms)')
pyplot.ylabel('Puissance du LASER (W/m2)')
pyplot.title('Comparaison des formes temporelles pour les impulsions lasers\n '
             'pour la fonction Porte et la fonction Gaussienne')
pyplot.show()
'''wavelength, ni, k = [], [], []
n = 0
# Ouvrir le fichier csv
with open('Ressources/index/Aluminium_index.csv', 'r') as f:
    # Créer un objet csv à partir du fichier
    obj = csv.reader(f, delimiter=',')

    for ligne in obj:
        if n > 0:
            print(n)
            wavelength.append(float(ligne[0] + '.' + ligne[1]))
            ni.append(float(ligne[2] + '.' + ligne[3]))
            k.append(float(ligne[4] + '.' + ligne[5]))
        n += 1


def f(x, wl, ni, k):
    tck = interpolate.splrep(wl, ni)
    tck2 = interpolate.splrep(wl, k)
    return [interpolate.splev(x, tck), interpolate.splev(x, tck2)]


print(f(0.8, wavelength, ni, k)[0])

pyplot.plot(wavelength, ni, 'o')
pyplot.plot(wavelength, f(wavelength, wavelength, ni, k)[0])
pyplot.show()'''

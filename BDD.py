# Fichier contenant une petite base donnée de matériau
# avec leur caractéristique propre

liste_materiaux = {
    'Aluminium': {  # caractéristique de l'aluminium
        'rho': 2698,  # masse volumique (kg/m3)
        'c': 900,  # Capacité thermique massique (J/kg/K)
        'K': 237,  # Conductivité thermique (W/m/K)
        'R': 0.87,  # Réflectance à 800 nm
    },
    'Graphite': {  # caractéristique du graphite ??
        'rho': 2250,  # masse volumique (kg/m3)
        'c': 720,  # Capacité thermique massique (J/kg/K)
        'K': 80,  # Conductivité thermique (W/m/K)
        'R': 0.17023,  # Réflectance à 800 nm
    },
    'Silicium': {  # caractéristique du silicium
        'rho': 1922,  # masse volumique (kg/m3)
        'c': 703,  # Capacité thermique massique (J/kg/K)
        'K': 149,  # Conductivité thermique (W/m/K)
        'R': 0.32940,  # Réflectance à 800 nm
        'nr': 3.6941,  # Indice réel
        'ni': 0.0065435,  # Indice imaginaire
    }
}
# Importation des modules nécessaires
from tkinter import *
from calcul_et_affichage_temperature import *


def center(win):
    # Fonction servant à centrer la fenêtre de l'UI sur l'écran
    """
    centers a tkinter window
    :param win: the main window or Toplevel window to center
    """
    win.update_idletasks()
    width = win.winfo_width()
    frm_width = win.winfo_rootx() - win.winfo_x()
    win_width = width + 2 * frm_width
    height = win.winfo_height()
    titlebar_height = win.winfo_rooty() - win.winfo_y()
    win_height = height + titlebar_height + frm_width
    x = win.winfo_screenwidth() // 2 - win_width // 2
    y = win.winfo_screenheight() // 2 - win_height // 2
    win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
    win.deiconify()


def calcul_ui(lambda_, tmax_UI, tmean_UI, fluence_UI, text_error, text_error_l, text_error_pt, text_error_II, label_CT,
              label_T, materiau, liste_materiaux, fct_int, zmax, tmax, I_in, cap_therm_bool, nom_graph_):
    if 150e-9 < float(lambda_) < 1.45e-6:
        # retirement de l'affichage d'erreur
        text_error.grid_forget()
        text_error_l.grid_forget()
        text_error_pt.grid_forget()
        text_error_II.grid_forget()

        # test des données sélectionnées
        print("Vous avez sélectionné : ", materiau)
        print("Longueur d'onde sélectionnée : ", lambda_)
        print("Fonction temporelle sélectionnée : ", fct_int)
        print("Nom du graphe choisi : ", nom_graph_)

        # ajout d'un espace au nom du graphique donné
        nom_graph = nom_graph_ + ''

        # appel des fonctions de calcul de température, de la carte et des courbes
        T, z, times, t_max, t_mean, fluence = calcul_temp(float(lambda_), liste_materiaux, fct_int, materiau, zmax,
                                                          tmax, I_in, cap_therm_bool)
        affichage_colormap(z, times, T, nom_graph)
        affichage_couche_10z(z, times, T, nom_graph)

        # affichage de la carte de température sur l'UI
        CarteTemperature = PhotoImage(file='Resultats/' + nom_graph_ + ' Carte Temperature.png')
        label_CT.config(image=CarteTemperature)
        label_CT.grid(row=4, column=0, columnspan=4, pady=5)

        # affichage des courbes dans le temps sur l'UI
        Temperatures = PhotoImage(file='Resultats/' + nom_graph_ + ' Courbes.png')
        label_T.config(image=Temperatures)
        label_T.grid(row=4, column=4, columnspan=4, pady=5)

        return t_max, t_mean, fluence

    else:
        # retirement de l'affichage d'erreur si besoin
        text_error.grid_forget()
        text_error_pt.grid_forget()
        text_error_II.grid_forget()
        # remise à zéro des températures obtenues
        tmax_UI.config(text='')
        tmean_UI.config(text='')
        fluence_UI.config(text='')
        # retirement de l'affichage des courbes en cas d'erreur
        label_T.grid_forget()
        label_CT.grid_forget()
        # affichage d'une erreur sur l'UI si les valeurs en entrées ne sont pas cohérentes
        text_error_l.grid(row=2, column=4, padx=5, pady=5)

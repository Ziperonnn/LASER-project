# Importation des modules nécessaires
from BDD import *
from UI import *
import re


def main_ui():
    # initialisation de l'affichage
    [root, bool_ct] = initialisation_UI()

    def action_button():
        # Obtenir les différentes entrées
        materiau = listeCombo_mat.get()
        fct_int = listeCombo_fct.get()
        lambda_ = wl_entry.get()
        profondeur_ = profondeur.get()
        nom_graph_ = nom_graph.get()
        temps_ = temps.get()
        I_in = Intensity_init.get()
        print(I_in)
        print(type(I_in))
        cap_therm_bool = bool_ct.get()

        # tests sur les différentes entrées profondeur et temps afin de vérifier leur cohérence
        if profondeur_ == "":
            profondeur_microm = 1e-3
        elif re.match("^[0-9]+[.]?[0-9]*[eE]?[-]?[0-9]*$", profondeur_):
            profondeur_microm = float(profondeur_) * 1e-3
        else:
            profondeur_microm = None

        if temps_ == "":
            temps_ms = 10e-3
        elif re.match("^[0-9]+[.]?[0-9]*[eE]?[-]?[0-9]*$", temps_):
            temps_ms = float(temps_) * 1e-3
        else:
            temps_ms = None

        # test sur l'entrée de l'intensité initiale du laser en W/m2
        if I_in == "" or I_in == " ":
            I_init = 10e6
        elif re.match("^[0-9]+[.]?[0-9]*[eE]?[-]?[0-9]*$", I_in):
            I_init = float(I_in)
        else:
            I_init = None

        if I_init is not None:
            # tests sur la valeur de la longueur d'onde choisie
            if re.match("^[0-9]+[.]?[0-9]*[eE]?[-]?[0-9]*$", lambda_):
                if profondeur_microm is not None and temps_ms is not None:
                    t_max, t_mean, fluence = calcul_ui(lambda_, tmax_UI, tmean_UI, fluence_UI, text_error, text_error_l,
                                                       text_error_pt, text_error_II, label_CT, label_T, materiau,
                                                       liste_materiaux, fct_int, profondeur_microm, temps_ms, I_init,
                                                       cap_therm_bool, nom_graph_)
                    tmax_UI.config(text=str(t_max))
                    tmean_UI.config(text=str(t_mean))
                    fluence_UI.config(text=str(fluence))

                else:
                    # retirement de l'affichage d'erreur si besoin
                    text_error.grid_forget()
                    text_error_l.grid_forget()
                    text_error_II.grid_forget()
                    # remise à zéro des températures obtenues
                    tmax_UI.config(text='')
                    tmean_UI.config(text='')
                    fluence_UI.config(text='')
                    # affichage d'une erreur sur l'UI si les valeurs en entrées ne sont pas cohérentes
                    text_error_pt.grid(row=2, column=4, padx=5, pady=5)
                    # retirement de l'affichage des courbes en cas d'erreur
                    label_T.grid_forget()
                    label_CT.grid_forget()

            else:
                # retirement de l'affichage d'erreur si besoin
                text_error_pt.grid_forget()
                text_error_l.grid_forget()
                text_error_II.grid_forget()
                # remise à zéro des températures obtenues
                tmax_UI.config(text='')
                tmean_UI.config(text='')
                fluence_UI.config(text='')
                # retirement de l'affichage des courbes en cas d'erreur
                label_T.grid_forget()
                label_CT.grid_forget()
                # affichage d'une erreur sur l'UI si les valeurs en entrées ne sont pas cohérentes
                text_error.grid(row=2, column=4, padx=5, pady=5)
        else:
            # retirement de l'affichage d'erreur si besoin
            text_error_pt.grid_forget()
            text_error_l.grid_forget()
            text_error.grid_forget()
            # remise à zéro des températures obtenues
            tmax_UI.config(text='')
            tmean_UI.config(text='')
            fluence_UI.config(text='')
            # retirement de l'affichage des courbes en cas d'erreur
            label_T.grid_forget()
            label_CT.grid_forget()
            # affichage d'une erreur sur l'UI si les valeurs en entrées ne sont pas cohérentes
            text_error_II.grid(row=2, column=4, padx=5, pady=5)

    # création des labels d’erreur :
    [text_error, text_error_l, text_error_pt, text_error_II] = erreur_UI(root)

    # création des labels et entrées pour le choix de l'utilisateur :
    [label_T, label_CT, temps, profondeur, Intensity_init, nom_graph,
     wl_entry, listeCombo_fct, listeCombo_mat] = affichage_entrée(root, bool_ct)

    # création des sorties
    tmax_UI, tmean_UI, fluence_UI = affichage_sortie(root)

    # création du bouton calculer et assignement à la commande de calcul
    button = Button(root, text="Calculer", command=action_button, bg='#1E5F74', fg='#FCDAB7')
    button.grid(row=1, column=6, padx=20, pady=5)

    root.mainloop()


if __name__ == '__main__':
    main_ui()

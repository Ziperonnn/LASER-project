from fonctions_calcul import *
from matplotlib import pyplot as plt


def calcul_temp(lambda_0, liste_BDD, fct_int, i, zmax, tmax, I0, cap_therm_bool):
    # Fonction servant à calculer la répartition de la chaleur dans un matériau choisi
    # en fonction de la longueur d'onde

    # initialisation des paramètres de départ
    Dt, Dz, z, tmax, times, T, T0 = initialisation(zmax, tmax)

    # création du vecteur température pour un temps t
    Told = T0 * np.ones(len(z))
    N = len(Told)
    k = 0

    # attribution des constantes des matériaux
    rho = liste_BDD[i]['rho']  # masse volumique (kg/m3)
    c = 1

    # Capacité thermique massique (J/kg/K)
    if cap_therm_bool == 1:
        c = liste_BDD[i]['c']
    else:
        if i == 'Aluminium':
            c = cp_alu(T0)
        elif i == 'Graphite':
            c = cp_graphite(T0)
        elif i == 'Silicium':
            c = cp_silicium(T0)

    K = liste_BDD[i]['K']  # Conductivité thermique (W/m/K)
    nr = calcul_indice(lambda_0 * 10 ** 6, i)[0]  # Indice réel
    ni = calcul_indice(lambda_0 * 10 ** 6, i)[1]  # Indice imaginaire
    if ni < 0.0001:
        ni = 0.0001  # Indice imaginaire modifiée si trop proche de zéro
    print('nr vaut : ', nr, ' et ni vaut : ', ni)

    # calcul de la constante de diffusion thermique
    Const = (K * Dt) / (rho * c * (Dz ** 2))

    Intensity_total = []

    # boucle de calcul de la carte de température
    for t in np.arange(0, tmax, Dt):
        if Const < 0.5:
            # calcul de l’intensité pour le temps n et en espace m
            if fct_int == 'Fonction Porte':
                I_0 = carree_int(t, 1.875e-3, 2.125e-3, I0)  # 2.75e-3, 5.25e-3, I0)
            else:
                I_0 = gaussian_int(t, I0 / 10, 2e-3, 0.0001)  # tmax / 2, 0.001)

            Intensity_total.append(I_0)
            Intensity = vect_intensity(I_0, z, lambda_0, nr, ni)

            # calcul de l'intensité pour le temps n et en espace m+1 // dernière valeur mise à 0 arbitrairement
            Intensity_1 = Intensity[1:len(Intensity)]
            Intensity_1 = np.append(Intensity_1, [0])

            # modification de la valeur de la capacité thermique en fonction de la température
            if cap_therm_bool == 0:
                if i == 'Aluminium':
                    c = cp_alu(Told)
                elif i == 'Graphite':
                    c = cp_graphite(Told)
                elif i == 'Silicium':
                    c = cp_silicium(Told)

            # calcul de la température en temps n+1 et en espace m
            Tnew = Told + (Dt * (Intensity - Intensity_1)) / (rho * Dz * c)
            Told = Tnew

            # calcul de la diffusion thermique
            Tnew[1:N - 1] = Told[1:N - 1] + Const * (Told[2:N] + Told[0:N - 2] - 2 * Told[1:N - 1])

            # calcul des conditions limites
            Tnew[0] = Told[0] + Const * (Told[1] - Told[0])
            Tnew[N - 1] = Told[N - 1] + Const * (Told[N - 2] - Told[N - 1])

            # ajout de la température en discrétisant par rapport au nombre de valeurs de temps
            if t >= times[k]:
                T[:, k] = Tnew
                k += 1
                Told = Tnew

    # Calcul de l'énergie du laser en W/m^2 * s * 10^-3
    Laser_Energy = np.trapz(Intensity_total, dx=1)
    Fluence_laser = round(Laser_Energy * 1e3 * 1e-12, 1)
    print('Energie totale générée par le laser  = ', Fluence_laser, 'TJ/m2')
    # On multiplie par 1e3 pour convertir notre énergie calculée en J.
    # Puis on multiplie aussi par 1e-6 deux fois pour convertir nos Joules en MJ [et les m2 en mm2.]

    # Calcul des deux sorties
    t_max = np.round(np.max(T), 1)
    t_mean = np.round(np.mean(T[-2]), 1)

    print('température max : ', t_max, 'K')
    print('température finale : ', t_mean, 'K')
    return T, z, times, t_max, t_mean, Fluence_laser


def affichage_colormap(z, times, T, nom):
    # Fonction qui affiche et sauvegarde le colormap de la température dans
    # le matériau en profondeur et en temps
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.pcolormesh(1e3 * z,  # profondeur en µm
                        1e3 * times,  # temps en ms
                        T[:len(z) - 1, :len(times) - 1].T,
                        cmap='magma',
                        )
    ax.invert_yaxis()
    ax.set_xlabel('Profondeur ($\mu{}m$)')
    ax.set_ylabel('Temps ($ms$)')
    ax.set_title('Température ($K$)')
    cbar = fig.colorbar(cax)

    fig.savefig('Resultats/' + nom + ' Carte Temperature.png')


def affichage_couche_10z(z, times, T, nom):
    # Fonction qui affiche et sauvegarde des courbes de température dans
    # le matériau en profondeur pour 10 intervalles de temps différents
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    ax2.set_xlabel('Profondeur ($\mu{}m$)')
    ax2.set_ylabel('Température ($K$)')
    Ncurves = 10
    for i in np.linspace(0, len(times) - 2, Ncurves):
        ax2.plot(1e6 * z,
                 T[:, int(i)], label=str(np.floor(times[int(i)] * 1e3)) + ' $ms$')
    ax2.legend()
    plt.title('Evolution de la température à 10 temps différents')

    fig2.savefig('Resultats/' + nom + ' Courbes.png')
